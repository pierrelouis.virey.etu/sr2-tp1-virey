package fil.sr2.auth;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class TokenManagerTest {
	private TokenManager tokenManager;
	private String token = "n4bd0seffr71epm17vln2vo9sg";
	
	@Before
	public void init() {
		tokenManager = TokenManager.getInstance();
		tokenManager.getTokenList().add(token);
	}
	
	@Test
	public void authorizeIsAuthorizingWhenGoodTokenTest() throws UnknownTokenException {
		tokenManager.authorize(token);
	}
	
	@Test(expected=UnknownTokenException.class)
	public void authorizeIsNotAuthorizingWhenBadTokenTest() throws UnknownTokenException {
		tokenManager.authorize("some bad token");
	}
}
