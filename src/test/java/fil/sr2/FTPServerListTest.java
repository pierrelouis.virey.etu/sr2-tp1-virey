package fil.sr2;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class FTPServerListTest {
	
	private FTPServerList serverList;
	private String ftp1;
	private String ftp2;
	private String name1;
	private String name2;
	
	@Before
	public void init() {
		ftp1 = "ftp.test.com";
		ftp2 = "ftp.test.gg";
		name1 = "ftp1";
		name2 = "ftp2";
		
		serverList = FTPServerList.getInstance();
		serverList.addServer(ftp1, name1, "anon", "secret", -1);
		serverList.addServer(ftp2, name2, "anon", "secret", -1);
	}
	
	@Test
	public void addServerIsAddingServerTest() {
		String ftp = "ftp3.test.fr";
		String name =  "ftp3";
		serverList.addServer(ftp, name, "anon", "secret", -1);
		assertTrue(serverList.getServerList().containsKey(name));
	}
	
	@Test
	public void addServerIsNotAddingServerIfNameExistInListTest() {
		String ftp = "ftp3.test.fr";
		boolean inserted = serverList.addServer(ftp, name1, "anon", "secret", -1);
		assertFalse(inserted);
	}
	
	@Test
	public void changeNameChangesTheNameTest() {
		String newName = "superTest";
		serverList.changeName(name1,newName);
		assertTrue(serverList.getServerList().containsKey(newName));
	}
	
	@Test
	public void changeNameDeleteOldNameTest() {
		String newName = "superTest";
		serverList.changeName(name1,newName);
		assertFalse(serverList.getServerList().containsKey(name1));
	}
	
	@Test
	public void deleteAllIsDeletingEverythingTest() {
		serverList.deleteAll();
		assertTrue(serverList.getServerList().isEmpty());
	}
	
	@Test
	public void deleteIsDeletingAnEntryTest() {
		serverList.deleteServer(name1);
		assertFalse(serverList.getServerList().containsKey(name1));
	}
	
	@Test
	public void deleteIsReturningFalseWhenServerDoesntExsitTest() {
		boolean res = serverList.deleteServer("UNKNOWN");
		assertFalse(res);
	}
	
}
