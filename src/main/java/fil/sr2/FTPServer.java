package fil.sr2;

public class FTPServer {
	
	private String URL;
	private String name;
	private String user;
	private String passwd;
	private int port;
	private boolean anonymous;
	
	/**
	 * Constructeur pour tous type de connexion.
	 */
	public FTPServer(String URL, String name, String user, String passwd, int port) {
		this.URL = URL;
		this.name = name;
		this.user = user;
		if(this.user.equals("anonymous"))
			anonymous = true;
		else 
			anonymous = false;
		this.passwd = passwd;
		this.port = port;
	}

	public String getURL() {
		return URL;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getPasswd() {
		return passwd;
	}
	
	public String getUser() {
		return user;
	}
	
	public int getPort() {
		return port;
	}
	
	public boolean getAnonymous() {
		return anonymous;
	}
	
	@Override
	public String toString() {
		return "FTPServer [name=" + name + ", URL=" + URL + ", user=" + user + ", port=" + port + ", anonymous=" + anonymous
				+ "]";
	}
}
