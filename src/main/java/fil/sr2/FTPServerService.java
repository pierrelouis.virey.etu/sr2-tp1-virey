package fil.sr2;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import fil.sr2.auth.Secured;

@Path("/ftp")
@Secured
public class FTPServerService extends FTPService {
	
	/**
	 * Renvoie la liste des serveurs FTP enregistrées dans la base de données.
	 * @return une reponse contenant les la liste
	 */
	@GET
	public Response getAllServer() {
		System.out.println("Affichage de la liste des serv FTP");
		String servList = serverList.listServer();
		return Response.status(200).entity(servList).build();
	}
	
	/**
	 * Renvoie les details d'un serveur précisé par son nom.
	 * @return la liste des serveurs en TEXT_PLAIN
	 */
	@GET
	@Path("/{servName}")
	public Response getServer(@PathParam("servName") String servName) {
		System.out.println("Affichage du serv FTP : "+servName);
		FTPServer serv = serverList.getServer(servName);
		if(serv == null)
			return Response.status(404).entity("This FTP server doesn't exist\n").build();
		String servInfos = serv.toString();
		return Response.status(200).entity(servInfos).build();
	}
	
	/**
	 * Modifie le nom serveur avec un nouveau.
	 * @return la liste des serveurs en TEXT_PLAIN
	 */
	@PUT
	@Path("/{servName}")
	public Response updateServer(@PathParam("servName") String servName,@QueryParam("newName") String newName) {
		System.out.println("Changement de "+servName+" en "+ newName + ".");
		
		if(serverList.changeName(servName,newName))
			return Response.status(201).entity("The resource as been renamed\n").build();
		else
			return Response.status(404).entity("The server "+servName+ " does not exist.\n").build();
	}
	
	@POST
	public Response addFTPServer(@QueryParam("servUrl") String servUrl, @QueryParam("servName") String servName, @QueryParam("user") String user, @QueryParam("passwd") String passwd, @QueryParam("port") int port) {
		System.out.println("Ajout de serveur");
		if(serverList.addServer(servUrl,servName,user,passwd,port))
			return Response.status(201).entity("The server "+ servName + " as been added\n").build();
		else
			return Response.status(400).entity("The resource already exist, please rename it or delete it\n").build();
	}
	
	@DELETE
	public Response deleteFTPServer() {
		serverList.deleteAll();
		return Response.status(200).entity("Suppression de tous les serveurs.\n").build();
	}

	@DELETE
	@Path("/{servName}")
	public Response deleteFTPServer(@PathParam("servName") String servName) {
		System.out.println("Deleting : "+servName);
		if (serverList.deleteServer(servName))
			return Response.status(200).entity("Le serveur : "+ servName +" est supprimé.\n").build();
		else
			return Response.status(404).entity("Le serveur : "+ servName +" n'existe pas.\n").build();
	}
}
