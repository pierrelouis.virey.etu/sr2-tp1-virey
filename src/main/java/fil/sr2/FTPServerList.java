package fil.sr2;

import java.util.HashMap;


/**
 * Singleton représentant tous les serveurs déjà enregistrées
 */
public class FTPServerList {
	/** Instance unique pré-initialisée */
	private static FTPServerList INSTANCE = new FTPServerList();
	private HashMap<String,FTPServer> serverList;
	
	/** Constructeur privé */
	private FTPServerList() {
		this.serverList = new HashMap<String,FTPServer>();
	}

	/** Point d'accès pour l'instance unique du singleton */
	public static FTPServerList getInstance() {
		return INSTANCE;
	}
	
	public HashMap<String, FTPServer> getServerList() {
		return serverList;
	}
	
	/**
	 * Ajoute un serveur à liste si un serveur portant ce nom n'existe pas.
	 * @return true si l'ajout à été fait sinon false.
	 */
	public boolean addServer(String servUrl, String servName,String user,String passwd, int port) {
		if(serverList.containsKey(servName))
			return false;
		
		FTPServer serv = new FTPServer(servUrl,servName,user,passwd,port);
		serverList.put(serv.getName(),serv);
		return true;
	}
	
	/**
	 * Supprime un server de la liste des serveurs
	 * @return true si la suppresion c'est bien passé, sinon false.
	 */
	public void deleteAll() {
		serverList.clear();
	}
	
	/**
	 * Supprime un server de la liste des serveurs
	 * @return true si la suppresion c'est bien passé, sinon false.
	 */
	public boolean deleteServer(String servName) {
		if(serverList.containsKey(servName)) {
			serverList.remove(servName);
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Change le nom d'un serveur oldName par newName
	 * @return true si la modification est faite faux si le serveurs n'existe pas.
	 */
	public boolean changeName(String oldName,String newName) {
		if(serverList.containsKey(oldName)) {
			FTPServer serv = serverList.get(oldName);
			serverList.remove(oldName);
			serv.setName(newName);
			serverList.put(serv.getName(),serv);
			return true;
		}
		return false;
	}
	
	/**
	 * Return the server list as a String.
	 */
	public String listServer() {
		if (this.serverList.isEmpty()) {
			return "Aucun serveur enregistree\n";
		} else {
			StringBuilder sb = new StringBuilder();
			for (String key : serverList.keySet()) {
				sb.append(serverList.get(key).toString());
				sb.append("\n");
			}
			return sb.toString();
		}
	}
	
	public FTPServer getServer(String servName) {
		return serverList.get(servName);
	}
	
}
