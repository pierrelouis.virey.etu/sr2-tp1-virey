package fil.sr2;

import java.io.IOException;

import org.apache.commons.net.ftp.FTPClient;

public abstract class FTPService {

	protected FTPServerList serverList = FTPServerList.getInstance();

	/**
	 * Renvoie un client de connexion correspondant au serv FTP associé au servName
	 * 
	 * @param servName le nom du serveur FTP
	 * @return FTPClient le client pour la connexion
	 */
	protected FTPClient connectServer(String servName) {
		FTPClient ftpClient = new FTPClient();

		FTPServer ftpInfo = serverList.getServer(servName);

		if (ftpInfo == null)
			return null;

		int port = ftpInfo.getPort();
		String servUrl = ftpInfo.getURL();

		try {
			if (port == -1)
				ftpClient.connect(servUrl);
			else
				ftpClient.connect(servUrl, port);

			ftpClient.login(ftpInfo.getUser(), ftpInfo.getPasswd());
		} catch (IOException e) {
			return null;
		}

		return ftpClient;
	}

	/**
	 * Ferme la connexion proprement avec le serveur.
	 */
	protected void closeConnexion(FTPClient ftpClient) {
		try {
			ftpClient.logout();
			ftpClient.disconnect();

		} catch (IOException e) {
			System.out.println("Echec de la fermeture de la connexion");
			// Rien de plus car la plateforme doit continuer à tourner.
		}
	}

}
