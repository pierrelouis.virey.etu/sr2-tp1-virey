package fil.sr2.auth;

import java.util.ArrayList;

public class TokenManager {
	/** Instance unique pré-initialisée */
	private static TokenManager INSTANCE = new TokenManager();
	private ArrayList<String> tokenList;
	
	/** Constructeur privé */
	private TokenManager() {
		this.tokenList = new ArrayList<String>();
		//Décomentez pour avoir un "cheat" et pas generer un cheat.
		this.tokenList.add("token");
	}

	/** Point d'accès pour l'instance unique du singleton */
	public static TokenManager getInstance() {
		return INSTANCE;
	}
	
	public ArrayList<String> getTokenList() {
		return tokenList;
	}
	
	/**
	 * Verifie que l'on connais le token.
	 * @throws UnknownTokenException si on ne le connais pas.
	 */
	public void authorize(String token) throws UnknownTokenException {
		if(!tokenList.contains(token))
			throw new UnknownTokenException();
	}
}
