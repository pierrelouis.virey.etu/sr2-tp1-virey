package fil.sr2.auth;

public class UnknownTokenException extends Exception {
	public UnknownTokenException() {
		super("A client tried to login with an unkonw token.");
	}
}
