package fil.sr2.auth;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Random;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import fil.sr2.FTPServerList;

@Path("/authentication")
public class AuthenticationEndpoint {
	private TokenManager tokens = TokenManager.getInstance();
	private String user = "Machin";		//Les credentials pour l'appli.
	private String passwd = "Truc123";
	
	/**
	 * Authentifie un utilisateur, si on le connais on créer un token
	 * @return Reponse contenant un token, sinon 403 forbidden.
	 */
	@POST
	public Response authenticateUser(@QueryParam("username") String username, @QueryParam("password") String password) {
		// Authentification avec les informations passées en arguments
		if (!authenticate(username, password))
			return Response.status(Response.Status.FORBIDDEN).build();

		// Création d'un token pour l'utilisateur
		String token = issueToken();
		// Renvoie le token dans la réponse
		return Response.ok(token).build();
	}
	
	/**
	 * Verifie les informations de l'utilisateur
	 */
	private boolean authenticate(String username, String password) {
		return (username.equals(user) && password.equals(passwd));
	}
	
	/**
	 * Création du token aléatoirement.
	 */
	private String issueToken() {
		Random random = new SecureRandom();
		String token = new BigInteger(130, random).toString(32);
		tokens.getTokenList().add(token);
		return token;
	}
}