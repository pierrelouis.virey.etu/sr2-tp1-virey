package fil.sr2;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.glassfish.jersey.media.multipart.FormDataParam;

import fil.sr2.auth.Secured;

@Path("/ftp/{servName}/file")
@Secured
public class FTPFileService extends FTPService {
	
	public static final String DIRECTORY_TRANSFER = "Directory transfer is not supported.";
	public static final String FILE_SYSTEM_ERROR = "Something went wrong trying to access the file.\n";
	public static final String FILE_NOT_EXIST = "The file doesn't exist on the machine.\n";
	public static final String BAD_TRANSFER = "Something went wrong during the transfer.\n";
	public static final String GOOD_TRANSFER = "Transfer complete.\n";
	public static final String ANONYMOUS_PERMISSION = "You cannot create or modify a file with an anonymous connection.\n";
	
	/**
	 * Extrait le nom du fichier à la fin du chemin
	 */
	private String extractFileName(String path) {
		String[] splitted = path.split("\\/");
		String fName = splitted[splitted.length-1];
		return fName;
	}
	
	/**
	 * Extrait le chemin de fichier avant de l'accèdez.
	 */
	private String extractFilePath(String path) {
		if(!path.contains("/"))
			return "";
		
		String[] splitted = path.split("\\/");
		StringBuilder fPath = new StringBuilder();
		for(int i=0; i<splitted.length-1; i++) {
			fPath.append(splitted[i]).append("/");
		}
		
		return fPath.toString();
	}
	
	/**
	 * Verifie que le fichier est pointé par filePath est un dossier ou non.
	 */
	private boolean isDirectory(FTPClient ftpClient, String filePath) throws IOException {
		ftpClient.enterLocalPassiveMode();
		FTPFile ftpFile = ftpClient.mlistFile(filePath);
		return ftpFile.isDirectory();
	}
	
	/**
	 * Permet de télécharger un fichier.
	 * @param servName le serveur contenant le fichier.
	 * @param filePath le chemin vers le fichier ou dossier.
	 * @return Reponse indiquant le succès ou non de l'opération.
	 */
	@GET
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Path("/{filePath: .*}")
	public Response retrieveFile(@PathParam("servName") String servName, @PathParam("filePath") String filePath) {
		System.out.println("On amorce le téléchargement de : " + filePath + " depuis : " + servName + ".");

		FTPClient ftpClient = connectServer(servName);
		System.out.println("Connected.");
		try {
			if (isDirectory(ftpClient, filePath)) { // Le fichier est un dossier
				return Response.status(500).entity(DIRECTORY_TRANSFER).build();
			} else { // il s'agit d'un fichier regulier
				String fileName = extractFileName(filePath);
				
				StreamingOutput fileStream = new StreamingOutput() {
					@Override
					public void write(java.io.OutputStream output) throws IOException, WebApplicationException {
						try {
							ftpClient.setFileType(FTP.BINARY_FILE_TYPE); // Fonctionne aussi pour un fichier text
							InputStream is = ftpClient.retrieveFileStream(filePath);
							// Lecture des data sous forme de byte de l'InputStream
							byte[] data = is.readAllBytes();
							// Excriture dans notre nouveau OutputStream.
							output.write(data);
							output.flush();
						} catch (Exception e) {
							throw new WebApplicationException("File Not Found !!");
						}
					}
				};
				//Renvoie de la réponse avec les données sous forme d'octet stream.
				return Response.ok(fileStream, MediaType.APPLICATION_OCTET_STREAM)
						.header("content-disposition", "attachment; filename = " + fileName).build();
			}
		} catch (IOException e) {
			closeConnexion(ftpClient);	// Fermeture propre de la connexion.
			return Response.status(500).entity(FILE_SYSTEM_ERROR).build();
		}
	}
	
	/**
	 * Upload le fichier précisé par l'inputStream sur la racine du serveur.
	 * @param ftpClient le client ftp.
	 * @param filePath le nom du fichier.
	 * @param inputStream l'inputStream vers le contenu du fichier.
	 * @return Reponse qui indique si le transfère c'est bien passé.
	 */
	private Response uploadFile(FTPClient ftpClient, String filePath, InputStream inputStream) {
		ftpClient.enterLocalPassiveMode();
		 
		try {
			ftpClient.setFileType(FTP.BINARY_FILE_TYPE);	//Type binaire fonctionne aussi avec fichier txt.
	        
	        //Ecriture du fichier.
			boolean done = ftpClient.storeFile(filePath, inputStream);
	        inputStream.close();
	        
	        if(!done) {
	        	closeConnexion(ftpClient);
	        	return Response.status(500).entity(BAD_TRANSFER).build();
	        }
	        
		} catch (FileNotFoundException e) {
			closeConnexion(ftpClient);
			return Response.status(500).entity(FILE_SYSTEM_ERROR).build();
		} catch (IOException e) {
			closeConnexion(ftpClient);
			return Response.status(500).entity(BAD_TRANSFER).build();
		}
		closeConnexion(ftpClient);
		return Response.status(200).entity(GOOD_TRANSFER).build();
	}
	
	/**
	 * Uplaod à la racine du serveur le fichier definie par l'input stream
	 * @param servName l'url du serveur à envoyer
	 * @param filePath le chemin du fichier sur la machine du client
	 * @return Reponse correspondant à l'état du transfer
	 */
	@POST
	@Path("/{filePath: .*}")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces({ MediaType.APPLICATION_OCTET_STREAM, MediaType.TEXT_PLAIN })
	public Response sendFile(@FormDataParam("file") InputStream is, @PathParam("servName") String servName, @PathParam("filePath") String filePath) {
		if(serverList.getServer(servName).getAnonymous())
			return Response.status(403).entity(ANONYMOUS_PERMISSION).build();
		
		System.out.println("On amorce l'uplaod dans : " + filePath + "sur le serveur : " + servName + ".");
		
		FTPClient ftpClient = connectServer(servName);
		
		return uploadFile(ftpClient, filePath, is);
	}
	
	/**
	 * Crée un dossier à l'endroit précisé par filePath.
	 * @return une Reponse indiquand le succès ou non de l'opération.
	 */
	@POST
	@Path("/mkdir/{filePath: .*}")
	public Response mkdir(@PathParam("servName") String servName, @PathParam("filePath") String filePath) {
		if(serverList.getServer(servName).getAnonymous())
			return Response.status(403).entity(ANONYMOUS_PERMISSION).build();
		
		FTPClient ftpClient = connectServer(servName);
		
		try {
			if(ftpClient.makeDirectory(filePath)) {
				closeConnexion(ftpClient);	// Fermeture propre de la connexion.
				return Response.status(200).entity(filePath+" is created.").build();
			} else {
				closeConnexion(ftpClient);	// Fermeture propre de la connexion.
				return Response.status(500).entity(FILE_SYSTEM_ERROR).build();
			}
		} catch (IOException e) {
			closeConnexion(ftpClient);	// Fermeture propre de la connexion.
			return Response.status(500).entity(FILE_SYSTEM_ERROR).build();
		}
	}
	
	/**
	 * Permet de renomer un fichier ou un dossier.
	 * @param newName le nouveau nom du fichier
	 * @param filePath le chemin d'accès au fichier
	 * @return une Reponse signifiant la réussite avec le code 200 sinon 500.
	 */
	@PUT
	@Path("/{filePath: .*}")
	public Response rename(@PathParam("servName") String servName, @PathParam("filePath") String filePath, @QueryParam("newName") String newName) {
		System.out.println(filePath + "  " + newName);
		
		if(serverList.getServer(servName).getAnonymous())
			return Response.status(403).entity(ANONYMOUS_PERMISSION).build();
		
		FTPClient ftpClient = connectServer(servName);
		
		String path = extractFilePath(filePath);
		String newFilePath = path + newName;
		
		try {
			if(ftpClient.rename(filePath, newFilePath)) {
				closeConnexion(ftpClient);	// Fermeture propre de la connexion.
				return Response.status(200).entity("File succesfull renamed.").build();
			}
			closeConnexion(ftpClient);	// Fermeture propre de la connexion.
			return Response.status(500).entity(FILE_SYSTEM_ERROR).build();
		} catch (IOException e) {
			closeConnexion(ftpClient);	// Fermeture propre de la connexion.
			return Response.status(500).entity(FILE_SYSTEM_ERROR).build();
		}
	}
	
	/**
	 * Algorithme recursif qui supprime le contenu d'un dossier en supprimant les
	 * sous-dossier et fichiers.
	 * @throws IOException
	 */
	public void deleteDirectory(FTPClient ftpClient, String targetDirectory) throws IOException {
		//Recupération de la liste de fichier du dossier courant
		FTPFile[] subFiles = ftpClient.listFiles(targetDirectory);

		if (subFiles != null && subFiles.length > 0) {
			for (FTPFile curFile : subFiles) {
				String curFileName = curFile.getName();
				if (curFileName.equals(".") || curFileName.equals("..")) {
					continue;	//On passe evidemment les dossier . et ..
				}
				//Contrscution du nom de la nouvelle cible.
				String subTarget = targetDirectory + "/" + curFileName;
				if (curFile.isDirectory()) {
					deleteDirectory(ftpClient, subTarget);
				} else {
					ftpClient.deleteFile(subTarget);
				}
			}
		}
		//Suppresion du dossier
		ftpClient.removeDirectory(targetDirectory);
	}
	
	/**
	 * Supprimme le fichier.
	 * @param servUrl l'url du serveur à envoyer
	 * @param filePath le chemin du fichier sur la machine du client
	 * @return Reponse correspondant à l'état du transfer
	 */
	@DELETE
	@Path("/{filePath: .*}")
	public Response deleteFile(@PathParam("servName") String servName, @PathParam("filePath") String filePath) {
		if(serverList.getServer(servName).getAnonymous())
			return Response.status(403).entity(ANONYMOUS_PERMISSION).build();
		
		FTPClient ftpClient = connectServer(servName);
		
		//On cherche à savoir si le fichier concerné est un dossier ou fichier
		boolean isDir;
		try {
			isDir = isDirectory(ftpClient, filePath);
		} catch (IOException e) {
			closeConnexion(ftpClient);	// Fermeture propre de la connexion.
			return Response.status(500).entity(FILE_SYSTEM_ERROR).build();
		}
		
		if(isDir) {
			try {
				deleteDirectory(ftpClient,filePath);
			} catch (IOException e) {
				closeConnexion(ftpClient);	// Fermeture propre de la connexion.
				return Response.status(500).entity("Cannot delete directory : "+filePath+".\n").build();
			}
			return Response.status(200).entity("Directory "+filePath+" is deleted.\n").build();
		} else
			try {
				ftpClient.deleteFile(filePath);
			} catch (IOException e) {
				closeConnexion(ftpClient);	// Fermeture propre de la connexion.
				return Response.status(500).entity("Cannot delete file : "+filePath+".\n").build();
			}
		closeConnexion(ftpClient);	// Fermeture propre de la connexion.
		return Response.status(200).entity("File "+filePath+" is deleted.\n").build();
	}
	
}
