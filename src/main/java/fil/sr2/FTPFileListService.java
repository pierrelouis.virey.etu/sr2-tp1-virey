package fil.sr2;

import java.io.IOException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;

import fil.sr2.auth.Secured;

@Path("/ftp/{servName}/file-list")
@Secured
public class FTPFileListService extends FTPService {
	
	/**
	 * Recupère la liste des fichiers
	 * @param servUrl le serveur à lister
	 * @param path le chemin à lister
	 * @return la reponse à envoyer au client
	 */
	private Response doList(String servName, String path) {
		FTPClient f = this.connectServer(servName);
		if(f==null)
			return Response.serverError().entity("Could not connect to server.\n").build();
		
		StringBuilder sb = new StringBuilder();
		
		try {
			f.enterLocalPassiveMode();

			FTPFile[] files = f.listFiles(path);
			for (FTPFile ftpFile : files) {
				// GODLIKE PROGRAMMING
				long time = ftpFile.getTimestamp().getTimeInMillis();		//L'heure avec 2h de retard. Donc j'en ajoute 2 à la mains
				long hourInMillis = 60 * 60 * 1000;
				time += hourInMillis*2;
				
				sb.append("Name: ").append(ftpFile.getName()).append(", ");
				sb.append("Path: ").append(path).append("/").append(ftpFile.getName()).append(", ");
				sb.append("Type: ").append(ftpFile.getType()).append(", ");
				sb.append("Timestamp: ").append(time).append("\n");
			}
		} catch (IOException e) {
			return Response.status(500).entity("Something happend while retrieving the list of files\n").build();
		}
		
		closeConnexion(f);
		
		String rep = sb.toString();
		return Response.status(200).entity(rep).build();
	}
	
	/**
	 * Execute la commande liste à la racine du serveur FTP.
	 * @param servUrl l'adresse du serveur FTP à lister.
	 * @return Response the response sent to the client.
	 */
	@GET
	public Response listHome(@PathParam("servName") String servName) {
		return doList(servName,"");
	}
	
	/**
	 * Execute la commande liste à la racine du serveur FTP.
	 * @param servUrl l'adresse du serveur FTP à lister.
	 * @param path le chemin à lister
	 * @return Reponse the reponse sent to the client.
	 */
	@GET
	@Path("/{path: .*}")
	public Response listRepository(@PathParam("servName") String servName,@PathParam("path") String path) {
		return doList(servName,path);
	}
	
}