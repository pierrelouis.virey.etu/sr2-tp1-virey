Application FlopBox : TP1 de SR2  
Virey Pierre-Louis  
31/03/21  

# Introduction

Cette API REST permet de centraliser les connexions à plusieurs serveurs FTP sur une même plate-forme.  
L'implémentation de ce programme utilise la librairie standard JAVA, JAX-RS pour le serveur, Apache commons pour la communication avec les serveurs FTP.  
La compilation du code se fait avec l'outil Apache Maven.  

# Vidéo de démonstration

 [![IMAGE ALT TEXT HERE](https://img.youtube.com/vi/1UeFKwa62Cc/0.jpg)](https://www.youtube.com/watch?v=1UeFKwa62Cc)

Le scénario de la vidéo sous forme de commandes est disponible dans le dossier doc/scenario_presentation.txt.  
J'ai essayé de completer toutes les exigences, seul la connexion active et l'envoie téléchargement de dossier ne sont pas implémenter. Dans ma "mauvaise" version du TP j'avais réussis
à faire le téléchargement/upload de dossier entier mais je n'ai pas re-essayé faut de temps.

# Usage et installation

## Installation

1. La compilation de ce programme est faite avec l'outil Apache maven pour l'obtenir : `apt install maven`
2. Cloner le repository : `git clone https://gitlab.univ-lille.fr/pierrelouis.virey.etu/sr2-tp1-virey.git`  
3. Déplacer vous dans le répertoire : `cd sr2-tp1-virey`  
4. Créer un exécutable : `mvn clean install` puis `mvn package`  
5. Pour lancer le programme utiliser simplement la commande, ensuite l'API est disponible à localhost:8080  
`java -jar target/flop-box-1.0-SNAPSHOT-jar-with-dependencies.jar`  

## Usage

#### Authentification et token

Voici une liste de commandes curl pour intéragir avec la plateforme. Vous pouvez aussi trouver les commandes de la vidéo dans le dossier doc.  
La connexion à la plateforme flop-box ce fait par token, il faut d'abord génèrer un token grâce à la commande suivante :  
`curl -v -X POST 'http://localhost:8080/flop-box/authentication?username=Machin&password=Truc123'`  
La commande renvoie un token que nous devons placer dans le header de chaque requête HTTP. Le mot de passe et l'identifiant de la plateforme sont Machin et Truc123 pour l'exemple.
Dans les parties suivantes on imagine que *token* représente le token génèré.

#### Opérations sur les serveurs : ajouter, supprimer, renommer un serveur

**Ajouter un serveur**:  
On a 5 informations dans les PathParams : l'url du serveur, le nom "l'alias" du serveur, l'username, le mot de passe et le port du serveur.  

Pour ajouter un serveur avec un port, et login / password il faut utiliser par exemple la commande suivante :  
`curl -v -H "Authorization: Bearer token" -X POST 'http://localhost:8080/flop-box/ftp?servUrl=127.0.0.1&servName=perso&user=user&passwd=12345&port=5000'`  
Pour un serveur anonyme il faut mettre en user anonymous.  
`curl -v -H "Authorization: Bearer token" -X POST 'http://localhost:8080/flop-box/ftp?servUrl=127.0.0.1&servName=persoAnon&user=anonymous&passwd=onsenfou&port=5000'`  
Pour se connecter à un url sans port il faut mettre le port à -1.  
`curl -v -H "Authorization: Bearer token" -X POST 'http://localhost:8080/flop-box/ftp?servUrl=ftp.ubuntu.com&servName=ubuntu&user=anonymous&passwd=onsenfou&port=-1'`

**Afficher la liste des serveurs**:  
`curl -v -H "Authorization: Bearer token" -X GET http://localhost:8080/flop-box/ftp/`

**Supprimer tous les serveurs**:  
`curl -v -H "Authorization: Bearer token" -X DELETE http://localhost:8080/flop-box/ftp`

**Supprimer le serveur perso par exemple**:  
`curl -v -H "Authorization: Bearer token" -X DELETE http://localhost:8080/flop-box/ftp/perso`

**Renomer un serveur, ici perso deviens local**:  
`curl -v -H "Authorization: Bearer token" -X PUT 'http://localhost:8080/flop-box/ftp/perso?newName=local'`

#### Lister les fichiers du serveur

On a au milieu de l'url le nom du serveur, l'alias que l'on a donné à l'adresse et ensuite le chemin du dossier à lister :  
`curl -v -H "Authorization: Bearer token" http://localhost:8080/flop-box/ftp/perso/file-list`  
`curl -v -H "Authorization: Bearer token" http://localhost:8080/flop-box/ftp/ubuntu/file-list`

#### Faire des opérations sur les fichiers du serveur

Attention si la connexion est anonyme on ne peut pas modifier les fichiers du serveur.

**Télécharger un fichier binaire ou texte**:  
Il faut indiquer le nom du serveur puis /file/ et ensuite le chemin vers le fichier. Dans les exempele on cherche les fichiers sur le serveur perso.  
`curl -v -H "Authorization: Bearer token" http://localhost:8080/flop-box/ftp/perso/file/text.txt --output text.txt`  
On télécharge le fichier avatar.png contenu dans le dossier hello.
`curl -v -H "Authorization: Bearer token" http://localhost:8080/flop-box/ftp/perso/file/hello/avatar.png --output avatar.png`  

**Envoyer un fichier binaire ou texte**:  
On envoie brouette.jpeg à la racine.  
`curl -v -H "Authorization: Bearer token" -F 'file=@brouette.jpeg' 'http://localhost:8080/flop-box/ftp/perso/file/brouette.jpeg'`  
On envoie le fichier darkest.txt dans le dossier hello.  
`curl -v -H "Authorization: Bearer token" -F 'file=@darkest.txt' 'http://localhost:8080/flop-box/ftp/perso/file/hello/darkest.txt'`  

**Créer un repertoire**:  
`curl -v -H "Authorization: Bearer token" -X POST http://localhost:8080/flop-box/ftp/perso/file/mkdir/gigaDir1`

**Supprimer un fichier ou un dossier complet**:  
`curl -v -H "Authorization: Bearer token" -X DELETE http://localhost:8080/flop-box/ftp/perso/file/avatar.png`  
`curl -v -H "Authorization: Bearer token" -X DELETE http://localhost:8080/flop-box/ftp/perso/file/dir`  

**Renommer un dossier ou fichier**:  
`curl -v -H "Authorization: Bearer token" -X PUT 'http://localhost:8080/flop-box/ftp/perso/file/hello/text.txt?newName=gigaText.txt'`

# Architecture

#### Architecture des fonctionnalités FTP

Voici le diagrammes des classes permettant les interactions avec le serveurs FTP et la gestion de serveur dans l'API :

![](doc/diag-uml-ftp.png)

- Les classes FTP---Service héritent de FTPService qui contient des méthodes basiques pour l'intéraction avec un serveur.  
- FTPServerService contient les url et méthodes pour stocker la liste de serveurs. 
- FTPFileListService contient les url et méthodes pour obtenir la liste de fichier d'un serveur.  
- FTPFileService contient toutes les url et méthodes pour faire opérations sur les fichiers du serveur.
- FTPserverList est un singletion qui contient la liste de serveur FTP eux-même représenté par la clasee FTPServer.  

#### Architecture des fonctionnalités d'Authentification

![](doc/diag-uml-auth.png)

Les classes ci-dessus permettent de créer l'annotation perso @Secured.
- AuthentificationEndPoint permet de renvoyer un token à un user qui souhaite se connecte.
- AuthentificationFilter permet de verifier le token de l'user avant chaque appel d'une méthode annotée @Secured.
- TokenManager est un singleton qui contient la liste des tokens.

### Gestion des erreurs:

Les erreurs sont assez souvent traitées pour envoyer une message d'erreur et ne pas terminer l'execution de la plateforme flop-box, c'est pourquoi en génèral à la detection d'une exception, on renvoie une code
d'erreur 500 à l'utilisateur expliquant ce qui s'est mal passé.
 
# Code Samples

## Le singleton FTPserverList  
Transformer la HashMap de server FTP en singleton a plusieurs avantages. Le code est plus sûre, les classes client ont un accès simple et unique à une seul instance et en plus les opérations 
sur la HashMap sont définie directement dans la classe ce qui ajoute une couche d'abstraction sur l'accès aux données.

```java
public class FTPServerList {
	/** Instance unique pré-initialisée */
	private static FTPServerList INSTANCE = new FTPServerList();
	private HashMap<String,FTPServer> serverList;
	
	/** Constructeur privé */
	private FTPServerList() {
		this.serverList = new HashMap<String,FTPServer>();
	}

	/** Point d'accès pour l'instance unique du singleton */
	public static FTPServerList getInstance() {
		return INSTANCE;
	}
	
	public HashMap<String, FTPServer> getServerList() {
		return serverList;
	}
	...
}
```

## La classe abstraite FTPService
La classe FTPService permet de pas redéfinir dans chaque classe fille l'accès à la liste de serveur ftpList ainsi que les méthodes pour se connecter et déconnecter d'un serveur FTP.

```java
public abstract class FTPService {

	protected FTPServerList serverList = FTPServerList.getInstance();

	/**
	 * Renvoie un client de connexion correspondant au serv FTP associé au servName
	 */
	protected FTPClient connectServer(String servName) {
		...

		return ftpClient;
	}

	/**
	 * Ferme la connexion proprement avec le serveur.
	 */
	protected void closeConnexion(FTPClient ftpClient) {
		...
	}

}
```

## Algorithme permettant la supression d'un dossier
L'algorithme ci-dessous permet la suppression d'un dossier ainsi que le contenu de ses sous-dossiers. C'est un algorithme recursif qui est executé sur l'ensemble des fichiers et sous-dossiers.

```java
public void deleteDirectory(FTPClient ftpClient, String targetDirectory) throws IOException {
		//Recupération de la liste de fichier du dossier courant
		FTPFile[] subFiles = ftpClient.listFiles(targetDirectory);

		if (subFiles != null && subFiles.length > 0) {
			for (FTPFile curFile : subFiles) {
				String curFileName = curFile.getName();
				if (curFileName.equals(".") || curFileName.equals("..")) {
					continue;	//On passe evidemment les dossier . et ..
				}
				//Contrscution du nom de la nouvelle cible.
				String subTarget = targetDirectory + "/" + curFileName;
				if (curFile.isDirectory()) {
					deleteDirectory(ftpClient, subTarget);
				} else {
					ftpClient.deleteFile(subTarget);
				}
			}
		}
		//Suppresion du dossier
		ftpClient.removeDirectory(targetDirectory);
	}
```

## Authentification par token
L'authentification à l'application se fait par token. L'utilisateur envoie son mot de passe et indentifiant à l'API qui va ensuite génèrer un token. Ce token doit ensuite être
placé dans le header des requêtes HTTP.

```java
@Path("/authentication")
public class AuthenticationEndpoint {
	
	/**
	 * Authentifie un utilisateur, si on le connais on créer un token
	 * @return Reponse contenant un token, sinon 403 forbidden.
	 */
	@POST
	public Response authenticateUser(@QueryParam("username") String username, @QueryParam("password") String password) {
		// Authentification avec les informations passées en arguments
		if (!authenticate(username, password))
			return Response.status(Response.Status.FORBIDDEN).build();

		// Création d'un token pour l'utilisateur
		String token = issueToken();
		// Renvoie le token dans la réponse
		return Response.ok(token).build();
	}
	
	// Verifie les informations de l'utilisateur
	private boolean authenticate(String username, String password) {...}
	
	//Créer un token aléatoire
	private String issueToken() {...}
}
```


## La création de l'annontation Secured
La création de l'annotation personnalisée @Secured permet une bonne optimisation du code. Elle permet de rediriger les requêtes vers chaque méthode ou classe annotées @Secured vers
notre moyen de vérification d'authentification personnel qui va ensuite vérifier le token passé en argument.

```java
//Déclaration de l'anotation
@NameBinding
@Retention(RUNTIME)
@Target({TYPE, METHOD})
public @interface Secured { }

//Déclaration de la méthode gérant les requêtes
@Secured
@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthentificationFilter implements ContainerRequestFilter { ... }

//Classes qui bénéficient de la redirection des requêtes pour vérifier le token.
@Path("/ftp/{servName}/file-list")
@Secured
public class FTPFileListService extends FTPService { ... }
```